rem %config IPCompleter.greedy=True
rem https://forums.fast.ai/t/jupyter-notebook-how-to-enable-intellisense/8636
rem python -m pip install --upgrade pip

docker run -i -t -p 8888:8888 continuumio/anaconda3 /bin/bash -c "/opt/conda/bin/conda install jupyter -y --quiet && mkdir /opt/notebooks && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root"

pause