
-- PLEASE CHANGE 'ROLLBACK' TO 'COMMIT' AT THE END OF THIS SCRIPT IF YOU READY MAKE CHANGES!

-- Estimated script execution time 1 min 18 second.
-- Estimated script output 73 591 rows.

-- The script makes changes:
/*
1. Remove duplicates with same English and same Chinese (perfect duplicates) in CHTERM against UNHQ.  
2. Extract excel list showing UNHQ record and CHTERM duplicate removed. Columns included should be: Dataset, English, Chinese, link to record.

3. Remove duplicates with same English and same Arabic (perfect duplicates) in ARABTERM against UNHQ.  Extract excel list as well.
4. Extract excel list showing UNHQ record and ARABTERM duplicate removed. Columns included should be: Dataset, English, Arabic, link to record.

5. Remove duplicates with same English and same Russian (perfect duplicates) in RUTERM against UNHQ.  Extract excel list as well.
6. Extract excel list showing UNHQ record and RUTERM duplicate removed. Columns included should be: Dataset, English, Russian, link to record
*/

SET NOCOUNT ON;
--GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
--GO

SET XACT_ABORT ON;  
--GO  

BEGIN TRAN

	DECLARE @LinkDomain NVARCHAR(1024) = 'https://test-conferences.unite.un.org'
	DECLARE @ReportType NVARCHAR(32) = '6-files-report' -- '3-files-report'

	DECLARE @MasterDataSource VARCHAR(30) = 'UNHQ';

	DECLARE @ServantDataSourceByLanguage TABLE(ReportNumber INT NOT NULL, ReportName NVARCHAR(50) NOT NULL, DataSourceID NVARCHAR(50) NOT NULL, [Language] NVARCHAR(50) NOT NULL)

	IF @ReportType = '6-files-report'
		-- 6-files report (73 591 rows):
		INSERT	@ServantDataSourceByLanguage VALUES 
			(1, 'UNHQ vs CHTERM by English',		'CHTERM',	'English'),	(2, 'UNHQ vs CHTERM by Chinese',		'CHTERM',	'Chinese'),
			(3, 'UNHQ vs ARABTERM by English',	'ARABTERM',	'English'), (4, 'UNHQ vs ARABTERM by Arabic',	'ARABTERM',	'Arabic'),
			(5, 'UNHQ vs RUTERM by English',		'RUTERM',	'English'),	(6, 'UNHQ vs RUTERM by Russian',		'RUTERM',	'Russian')
	ELSE
		-- 3-files report (55 625 rows):
		INSERT	@ServantDataSourceByLanguage VALUES 
			(1, 'UNHQ vs CHTERM',	'CHTERM',	'English'),	(1, 'UNHQ vs CHTERM',	'CHTERM',	'Chinese'),
			(2, 'UNHQ vs ARABTERM', 'ARABTERM',	'English'), (2, 'UNHQ vs ARABTERM', 'ARABTERM',	'Arabic'),
			(3, 'UNHQ vs RUTERM',	'RUTERM',	'English'),	(3, 'UNHQ vs RUTERM',	'RUTERM',	'Russian')

	-- Prepare table with computed Term hash for compare:
	IF OBJECT_ID('tempdb..#TermsNormalized') IS NOT NULL
		DROP TABLE #TermsNormalized

	CREATE TABLE #TermsNormalized(
		TermID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY NONCLUSTERED, 
		[Language] NVARCHAR(50) NOT NULL,
		TermHash VARBINARY(16) NOT NULL,
		DataSourceID NVARCHAR(50) NOT NULL,
		RecordID UNIQUEIDENTIFIER NOT NULL,
		[Space] NVARCHAR(50) NULL) 
	CREATE CLUSTERED INDEX	IX_TermsNormalized_TermHash		ON #TermsNormalized	(TermHash)
	CREATE INDEX			IX_TermsNormalized_DataSourceID	ON #TermsNormalized	(DataSourceID)
	CREATE INDEX			IX_TermsNormalized_Language		ON #TermsNormalized	([Language])

	--SELECT [Space], COUNT(*) _count FROM dbo.Record GROUP BY [Space] ORDER BY [Space]

	INSERT #TermsNormalized -- 912 246 rows at 35 second
	(
		TermID,
		[Language],
		TermHash,
		DataSourceID,
		RecordID,
		[Space]
	)
	SELECT	--t.Term, 
			t.TermID,
			t.[Language],
			HASHBYTES('MD5', 
				REPLACE(REPLACE(t.Term, ' ', ''), '-', '')	-- disregard double spaces, hyphens and capitalization (COLLATE is already = Latin1_General_CS_AS_KS_WS)
			) AS TermHash, -- Term field is too big for indexing as is, so hash used for indexing
			r.DataSourceID,
			t.RecordID,
			r.[Space]
	FROM [dbo].[Term] t
	JOIN [dbo].[Record] r
		ON t.RecordID = r.RecordID
	WHERE		r.DataSourceID IN (SELECT DISTINCT DataSourceID From @ServantDataSourceByLanguage UNION SELECT @MasterDataSource)
		  AND	t.[Language] IN (SELECT DISTINCT [Language] From @ServantDataSourceByLanguage)
		  AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
		  AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
		  AND	r.[Space] NOT IN ('deletedduplicate', 'deleted');
	--SELECT * FROM #TermsNormalized
	--SELECT r.* FROM #TermsNormalized JOIN dbo.Record r ON r.RecordID = #TermsNormalized.RecordID WHERE r.Space IN ('deletedduplicate', 'deleted')
	
	IF OBJECT_ID('tempdb..#OutputReport') IS NOT NULL
		DROP TABLE #OutputReport

	-- Prepare table for report about records affected:
	CREATE TABLE #OutputReport(
		ReportNumber INT NOT NULL,
		ReportName NVARCHAR(50) NOT NULL,
		TermID UNIQUEIDENTIFIER NOT NULL,
		[Language] NVARCHAR(50) NOT NULL,
		TermHash VARBINARY(16) NOT NULL,
		DataSourceID NVARCHAR(50) NOT NULL,
		RecordID UNIQUEIDENTIFIER NOT NULL,
		[Space] NVARCHAR(50) NULL,
		SpaceInserted NVARCHAR(50) NULL) 
	CREATE CLUSTERED INDEX	IX_OutputReport_DataSourceID_RecordID_ReportNumber	ON #OutputReport (DataSourceID, RecordID, ReportNumber)
	CREATE INDEX			IX_OutputReport_Language							ON #OutputReport ([Language])
	CREATE INDEX			IX_OutputReport_TermID								ON #OutputReport (TermID)
	CREATE INDEX			IX_OutputReport_TermHash							ON #OutputReport (TermHash)
	CREATE UNIQUE INDEX		IX_OutputReport_TermID_ReportNumber					ON #OutputReport (TermID, ReportNumber)

	DECLARE @ReportNumber INT
	DECLARE @ReportName NVARCHAR(50)
	DECLARE @ServantDataSource NVARCHAR(50)
	DECLARE @Language NVARCHAR(50)

	DECLARE ServantDataSourceByLanguage_cursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT ReportNumber, ReportName, DataSourceID, [Language] FROM @ServantDataSourceByLanguage 
	OPEN ServantDataSourceByLanguage_cursor  
	FETCH NEXT FROM ServantDataSourceByLanguage_cursor INTO @ReportNumber, @ReportName, @ServantDataSource, @Language; 

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		print ''
		print 'dbo.Record update, set [Space] to deletedduplicate for ' + @ServantDataSource + ' : ' + @Language + ' : ' + @ReportName

		UPDATE dbo.Record SET
	--	SELECT
			[Space] = 'deletedduplicate'
			-- insert Servant terms to report:
			OUTPUT @ReportNumber, @ReportName, n.TermID, n.[Language], n.TermHash, inserted.DataSourceID, inserted.RecordID, n.[Space], inserted.[Space] AS SpaceInserted INTO #OutputReport 
			FROM dbo.Record r
			JOIN #TermsNormalized n 
				ON n.RecordID = r.RecordID
			WHERE n.[Language] = @Language AND n.DataSourceID = @ServantDataSource
			AND EXISTS
				(
					SELECT TermHash FROM #TermsNormalized WHERE [Language] = @Language AND TermHash = n.TermHash AND DataSourceID = @ServantDataSource
					INTERSECT
					SELECT TermHash FROM #TermsNormalized WHERE [Language] = @Language AND TermHash = n.TermHash AND DataSourceID = @MasterDataSource
				)
			--AND Record.[Space] NOT IN ('deletedduplicate', 'deleted');

		-- insert Master terms to report:
		INSERT #OutputReport 
		SELECT
			@ReportNumber, @ReportName, n.TermID, n.[Language], n.TermHash, n.DataSourceID, n.RecordID, n.[Space], NULL AS SpaceInserted
			FROM dbo.Record r
			JOIN #TermsNormalized n 
				ON n.RecordID = r.RecordID
			WHERE n.[Language] = @Language AND n.DataSourceID = @MasterDataSource
			AND EXISTS
				(
					SELECT TermHash FROM #TermsNormalized WHERE [Language] = @Language AND TermHash = n.TermHash AND DataSourceID = @ServantDataSource
					INTERSECT
					SELECT TermHash FROM #TermsNormalized WHERE [Language] = @Language AND TermHash = n.TermHash AND DataSourceID = @MasterDataSource
				);

		FETCH NEXT FROM ServantDataSourceByLanguage_cursor INTO @ReportNumber, @ReportName, @ServantDataSource, @Language; 
	END

	CLOSE ServantDataSourceByLanguage_cursor;  
	DEALLOCATE ServantDataSourceByLanguage_cursor;  

	-- 49 214 records:
	--SELECT o.* FROM #OutputReport o JOIN dbo.Record r ON r.RecordID = o.RecordID WHERE o.SpaceInserted IN ('deletedduplicate')

	-- Final output for Excel file (ReportNumber is number of file):
	;WITH 
		CTE_OutputReport AS
		(
			SELECT 
				o.ReportNumber, o.ReportName, o.DataSourceID, o.RecordID, o.[Language], o.TermHash, t.Term, 
				o.[Space], r.[Space] NewSpace,
				CONCAT(@LinkDomain, '/UNTERM/Display/record/', o.DataSourceID, '/NA/', o.RecordID) AS RecordLink
			FROM #OutputReport o
			JOIN dbo.Term t ON t.TermID = o.TermID
			JOIN dbo.Record r ON r.RecordID = t.RecordID
		),
		CTE_Records AS
		(
			SELECT DataSourceID, RecordID, RecordLink, [Space], NewSpace, ReportNumber, ReportName
			FROM CTE_OutputReport
			GROUP BY DataSourceID, RecordID, RecordLink, [Space], NewSpace, ReportNumber, ReportName
		),
		CTE_English AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash EnglishHash, Term English FROM CTE_OutputReport WHERE [Language] = 'English'			
		),
		CTE_Russian AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash RussianHash, Term Russian FROM CTE_OutputReport WHERE [Language] = 'Russian'			
		),
		CTE_Arabic AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash ArabicHash, Term Arabic FROM CTE_OutputReport WHERE [Language] = 'Arabic'			
		),
		CTE_Chinese AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash ChineseHash, Term Chinese FROM CTE_OutputReport WHERE [Language] = 'Chinese'			
		),
		CTE_Report AS
		(
			SELECT 
			--ROW_NUMBER() OVER(ORDER BY f.ReportNumber, e.EnglishHash, CASE f.DataSourceID WHEN 'UNHQ' THEN NULL ELSE f.DataSourceID END, f.RecordID) RowNumber,
			f.ReportNumber [Report Number], f.ReportName [Report Name],
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber ORDER BY e.EnglishHash)	- 1	AS [English Group],
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber ORDER BY r.RussianHash)	- 1	AS [Russian Group],
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber ORDER BY a.ArabicHash)	- 1	AS [Arabic Group],
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber ORDER BY c.ChineseHash)	- 1	AS [Chinese Group],

			-- DISTINCT COUNT emulation (for same in English but difference in ...):
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English), COALESCE(r.RussianHash, RussianNonDouble.Russian)) 
			+ DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English) DESC, COALESCE(r.RussianHash, RussianNonDouble.Russian) DESC) 
			- 1
			AS [Russian Group DISTINCT Count],

			-- DISTINCT COUNT emulation (for same in English but difference in ...):
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English), COALESCE(a.ArabicHash, ArabicNonDouble.Arabic)) 
			+ DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English) DESC, COALESCE(a.ArabicHash, ArabicNonDouble.Arabic) DESC) 
			- 1
			AS [Arabic Group DISTINCT Count],

			-- DISTINCT COUNT emulation (for same in English but difference in ...):
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English), COALESCE(c.ChineseHash, ChineseNonDouble.Chinese)) 
			+ DENSE_RANK() OVER(PARTITION BY f.ReportNumber, COALESCE(e.EnglishHash, EnglishNonDouble.English) 
				ORDER BY COALESCE(e.EnglishHash, EnglishNonDouble.English) DESC, COALESCE(c.ChineseHash, ChineseNonDouble.Chinese) DESC) 
			- 1
			AS [Chinese Group DISTINCT Count],

			/*
			-- DISTINCT COUNT emulation:
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber, e.EnglishHash ORDER BY e.EnglishHash, ISNULL(r.RussianHash, NEWID())) 
			+ DENSE_RANK() OVER(PARTITION BY f.ReportNumber, e.EnglishHash ORDER BY e.EnglishHash DESC, ISNULL(r.RussianHash, NEWID()) DESC) 
			- 1
			AS [Russian Group DISTINCT Count 0],
			*/

			f.DataSourceID AS [Database], 
			f.[Space], f.NewSpace,
			--f.RecordID, 
			COALESCE(e.English, EnglishNonDouble.English) AS English, 
			COALESCE(r.Russian, RussianNonDouble.Russian) AS Russian, 
			COALESCE(c.Chinese, ChineseNonDouble.Chinese) AS Chinese, 
			COALESCE(a.Arabic, ArabicNonDouble.Arabic) AS Arabic, 
			e.EnglishHash, r.RussianHash, a.ArabicHash, c.ChineseHash,
			[Subject].[Subject], [Body].[Body],
			f.RecordLink 
			FROM CTE_Records f
			LEFT JOIN CTE_English	e ON e.DataSourceID = f.DataSourceID AND e.RecordID = f.RecordID AND e.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Russian	r ON r.DataSourceID = f.DataSourceID AND r.RecordID = f.RecordID AND r.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Arabic	a ON a.DataSourceID = f.DataSourceID AND a.RecordID = f.RecordID AND a.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Chinese	c ON c.DataSourceID = f.DataSourceID AND c.RecordID = f.RecordID AND c.ReportNumber = f.ReportNumber
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(s.[Name], ', ')
					FROM dbo.[Subject] s
					JOIN  dbo.RecordSubject rs 
						ON rs.SubjectID = s.SubjectID
					WHERE rs.RecordID = f.RecordID
					FOR XML PATH('') 
					) [Subject]
				) [Subject]
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(b.[Name], ', ')
					FROM dbo.Body b
					JOIN  dbo.RecordBody rb 
						ON rb.BodyID = b.BodyID
					WHERE rb.RecordID = f.RecordID
					FOR XML PATH('') 
					) [Body]
				) [Body]
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'English'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
						AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
						--AND	r.[Space] NOT IN ('deletedduplicate', 'deleted')
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) English
				) EnglishNonDouble
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Russian'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
						AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
						--AND	r.[Space] NOT IN ('deletedduplicate', 'deleted')
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Russian
				) RussianNonDouble
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Chinese'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
						AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
						--AND	r.[Space] NOT IN ('deletedduplicate', 'deleted')
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Chinese
				) ChineseNonDouble
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Arabic'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
						AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
						--AND	r.[Space] NOT IN ('deletedduplicate', 'deleted')
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Arabic
				) ArabicNonDouble

		--ORDER BY f.ReportNumber, e.EnglishHash, CASE f.DataSourceID WHEN 'UNHQ' THEN NULL ELSE f.DataSourceID END, f.RecordID
		--ORDER BY f.ReportNumber, r.RussianHash, CASE f.DataSourceID WHEN 'UNHQ' THEN NULL ELSE f.DataSourceID END, f.RecordID
		--ORDER BY f.ReportNumber, c.ChineseHash, CASE f.DataSourceID WHEN 'UNHQ' THEN NULL ELSE f.DataSourceID END, f.RecordID
		--ORDER BY f.ReportNumber, a.ArabicHash, CASE f.DataSourceID WHEN 'UNHQ' THEN NULL ELSE f.DataSourceID END, f.RecordID
		)
		SELECT 
		 CASE WHEN [Russian Group DISTINCT Count]	> 1 THEN 1 ELSE 0 END AS [Russian Differences]
		,CASE WHEN [Arabic Group DISTINCT Count]	> 1 THEN 1 ELSE 0 END AS [Arabic Differences]
		,CASE WHEN [Chinese Group DISTINCT Count]	> 1 THEN 1 ELSE 0 END AS [Chinese Differences]
		,[Report Number]
		,[Report Name]	
		,[English Group] + 1 AS [English Group]
		,[Russian Group] + 1 AS [Russian Group]
		,[Arabic Group]	 + 1 AS [Arabic Group]
		,[Chinese Group] + 1 AS [Chinese Group]
		,[Database]	
		,[Space]
		,NewSpace	
		,English
		,Russian		
		,Arabic
		,Chinese	
		--,EnglishHash	
		--,RussianHash	
		--,ArabicHash	
		--,ChineseHash	
		,[Subject]
		,Body
		,RecordLink
		FROM CTE_Report r
		ORDER BY r.[Report Number], r.[English Group], CASE r.[Database] WHEN 'UNHQ' THEN NULL ELSE r.[Database] END, r.RecordLink

	DROP TABLE #TermsNormalized
	DROP TABLE #OutputReport

--	ROLLBACK
COMMIT

/*
-- Restore from snapshot after start script if needed:
USE master
GO
ALTER DATABASE [UNTERM3] SET OFFLINE WITH ROLLBACK IMMEDIATE
GO
ALTER DATABASE [UNTERM3] SET ONLINE
GO
RESTORE DATABASE UNTERM3 FROM DATABASE_SNAPSHOT = 'UNTERM3_SNAPSHOT';  
GO  
*/
