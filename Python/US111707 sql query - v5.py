# pip install pymssql
# pip install openpyxl
# pip install pandas

data_source = "csv"
# data_source = "sql"

server = "."
database = "UNTERM3"
user = "gtextdev"
password = "^[~A(rjWc;&4"

sqlFile = "US111707 sql query - v5.sql"
csvFile = "US111707 sql query - v5.csv"

report_0 = "US111707 - single file report.xlsx"

report_RE = "US111707 - UNHQ vs RUTERM.xlsx"
report_AE = "US111707 - UNHQ vs ARABTERM.xlsx"
report_CE = "US111707 - UNHQ vs CHTERM.xlsx"

from sys import exit
from pathlib import Path

if data_source == "sql":
    inputFilePath = Path(sqlFile)
    if not inputFilePath.is_file():
        print(f"{sqlFile} not found")
        exit(1)

if data_source == "csv":
    inputFilePath = Path(csvFile)
    if not inputFilePath.is_file():
        print(f"{csvFile} not found")
        exit(1)

import numpy as np
import pandas as pd
from pandas import DataFrame


def to_excel(data: DataFrame, report: str, link_column: str):
    from openpyxl import load_workbook
    wb = load_workbook(filename=report.replace(".", " (template)."))
    ws = wb.worksheets[0]
    from openpyxl.utils.dataframe import dataframe_to_rows
    for r in dataframe_to_rows(data, index=False, header=False):
        ws.append(r)
    # hyperlinks:
    link_column_index = ws[f"{link_column}1"].col_idx + 1
    index: int = 1
    for row in ws.iter_rows(min_col=link_column_index, max_col=link_column_index, min_row=2, max_row=ws.max_row):
        for cell in row:
            index += 1
            cell.value = f'=HYPERLINK(""&{link_column}{index})'
    wb.save(report)
    wb.close()


if data_source == "sql":
    import pymssql
    with open(sqlFile) as file:
        sql = file.read()
        # print(sql)
    with pymssql.connect(server=server, database=database, user=user, password=password) as connection:
        data = pd.read_sql(sql, connection)
        connection.commit()
        # cursor = connection.cursor(as_dict=True)
        # cursor.execute("SELECT * FROM Datasource")
        # print( cursor.fetchall() )
        # for row in cursor:
        #      print(row["DatasourceId"])

if data_source == "csv":
        data = pd.read_csv(csvFile, encoding="utf-8", low_memory=False)
        data = (
            data[data["OutputDescription"] == "Report"]
            [["Duplicate Type", "Report Number", "Report Name", "English Group", "Database", "Space", "NewSpace", "English", "Russian", "Chinese", "Arabic", "Subject", "Body", "RecordLink", "RecordID"]]
        )

# 30 547
print(data["RecordID"].count())
to_excel(data, report_0, "N")

# 17 566
dataRE = (
    (data[data["Report Name"] == "UNHQ vs RUTERM"]) \
        [["Duplicate Type", "English Group", "Database", "Space", "NewSpace", "English", "Russian", "Subject", "Body", "RecordLink"]] \
    ).sort_values(by="English Group")
print(dataRE["RecordLink"].count())
to_excel(dataRE, report_RE, "J")

# 6 912
dataAE = (
    (data[data["Report Name"] == "UNHQ vs ARABTERM"]) \
        [["Duplicate Type", "English Group", "Database", "Space", "NewSpace", "English", "Arabic", "Subject", "Body", "RecordLink"]] \
    ).sort_values(by="English Group")
print(dataAE["RecordLink"].count())
to_excel(dataAE, report_AE, "J")

# 6 069
dataCE = (
    (data[data["Report Name"] == "UNHQ vs CHTERM"]) \
        [["Duplicate Type", "English Group", "Database", "Space", "NewSpace", "English", "Chinese", "Subject", "Body", "RecordLink"]] \
    ).sort_values(by="English Group")
print(dataCE["RecordLink"].count())
to_excel(dataCE, report_CE, "J")
