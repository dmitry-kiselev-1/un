
-- PLEASE CHANGE 'ROLLBACK' TO 'COMMIT' AT THE END OF THIS SCRIPT IF YOU READY MAKE CHANGES!

-- Estimated script execution time 42 second.
-- Estimated script output 30 547 rows report data and 10 239 rollback data.

-- The script makes changes:
/*
1. Remove duplicates with same English and same Chinese (perfect duplicates) in CHTERM against UNHQ.  
2. Extract excel list showing UNHQ record and CHTERM duplicate removed. Columns included should be: Dataset, English, Chinese, link to record.

3. Remove duplicates with same English and same Arabic (perfect duplicates) in ARABTERM against UNHQ.  Extract excel list as well.
4. Extract excel list showing UNHQ record and ARABTERM duplicate removed. Columns included should be: Dataset, English, Arabic, link to record.

5. Remove duplicates with same English and same Russian (perfect duplicates) in RUTERM against UNHQ.  Extract excel list as well.
6. Extract excel list showing UNHQ record and RUTERM duplicate removed. Columns included should be: Dataset, English, Russian, link to record

Perfect duplicates: same term/title English entry and same term/title language equivalents in all datasets.
Imperfect duplicates: same term/title English entry and discrepancies in other language equivalents.
*/

SET NOCOUNT ON;
--GO

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
--GO

SET XACT_ABORT ON;  
--GO  

-- the PowerShell single file output:
DECLARE @Output TABLE
(
    OutputDescription NVARCHAR(50) NOT NULL,
	-- 'Report':
    [Duplicate Type] NVARCHAR(50),
    [Report Number] INT,
    [Report Name] NVARCHAR(50),
    [English Group] INT,
    [Database] NVARCHAR(50),
    [Space] NVARCHAR(50),
    NewSpace NVARCHAR(50),
    English NVARCHAR(MAX),
    Russian NVARCHAR(MAX),
    Chinese NVARCHAR(MAX),
    Arabic NVARCHAR(MAX),
    [Subject] NVARCHAR(MAX),
    Body NVARCHAR(MAX),
    RecordLink NVARCHAR(MAX),
    RecordID UNIQUEIDENTIFIER,
    -- 'EmergencyRollback':
	[deleted_Space] NVARCHAR(50),
    [deleted_Updated] DATETIME,
    [inserted_Space] NVARCHAR(50),
    [inserted_Updated] DATETIME
);

BEGIN TRAN

	DECLARE @Domain NVARCHAR(128) = 'https://conferences.unite.un.org'

	DECLARE @DataSources TABLE(ReportNumber INT PRIMARY KEY, ReportName NVARCHAR(50) NOT NULL, DataSourceID NVARCHAR(50) NOT NULL, [Language] NVARCHAR(50) NOT NULL)

	INSERT	@DataSources VALUES 
		 (1, 'UNHQ vs RUTERM',		'RUTERM',	'Russian')
		,(2, 'UNHQ vs ARABTERM',	'ARABTERM',	'Arabic')
		,(3, 'UNHQ vs CHTERM',		'CHTERM',	'Chinese')

	-- Table for compare Terms by hash (it takes much hours comparing by Terms directly):
	IF OBJECT_ID('tempdb..#TermsNormalized') IS NOT NULL
		DROP TABLE #TermsNormalized
	CREATE TABLE #TermsNormalized(
		TermID UNIQUEIDENTIFIER NOT NULL PRIMARY KEY NONCLUSTERED, 
		TermHash VARBINARY(16) NOT NULL,
		DataSourceID NVARCHAR(50) NOT NULL,
		[Language] NVARCHAR(50) NOT NULL,
		RecordID UNIQUEIDENTIFIER NOT NULL) 
	
	CREATE CLUSTERED INDEX	IX_TermsNormalized_TermHash					ON #TermsNormalized	(TermHash)
	CREATE INDEX			IX_TermsNormalized_DataSourceID_Language	ON #TermsNormalized	(DataSourceID, [Language])

	INSERT #TermsNormalized -- 848 397 rows at 38 second
	(
		TermID,
		TermHash,
		DataSourceID,
		[Language],
		RecordID
	)
	SELECT	--t.Term, 
			t.TermID,
			HASHBYTES('MD5', 
				REPLACE(REPLACE(LOWER(LEFT(t.Term, 4000)), ' ', ''), '-', '')	-- disregard double spaces, hyphens and capitalization
				--COLLATE SQL_Latin1_General_CP1_CI_AS
				--COLLATE Latin1_General_CI_AS
			) AS TermHash, -- Term field is too big for indexing as is, so hash used for indexing
			r.DataSourceID,
			t.[Language],
			t.RecordID
	FROM [dbo].[Term] t
	JOIN [dbo].[Record] r
		ON t.RecordID = r.RecordID
	WHERE		r.DataSourceID IN (SELECT DataSourceID From @DataSources UNION SELECT 'UNHQ')
		  AND	t.[Language] IN (SELECT [Language] From @DataSources UNION SELECT 'English')
		  AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
		  AND	r.RecordType IN ('term', 'title', 'phraseology', 'footnote', 'proper name') -- term, title, country, footnote, phraseology, proper name
		  AND	r.[Space] NOT IN ('deletedduplicate', 'deleted');
	--SELECT * FROM #TermsNormalized ORDER BY RecordID 

	IF OBJECT_ID('tempdb..#Duplicates') IS NOT NULL
		DROP TABLE #Duplicates
	CREATE TABLE #Duplicates(
		DuplicateType NVARCHAR(50) NOT NULL,
		ReportNumber INT NOT NULL,
		ReportName NVARCHAR(50) NOT NULL,
		TermID UNIQUEIDENTIFIER NOT NULL,
		TermHash VARBINARY(16) NOT NULL,
		DataSourceID NVARCHAR(50) NOT NULL,
		[Language] NVARCHAR(50) NOT NULL,
		RecordID UNIQUEIDENTIFIER NOT NULL) 

	DECLARE @ReportNumber INT
	DECLARE @ReportName NVARCHAR(50)
	DECLARE @DataSource NVARCHAR(50)
	DECLARE @Language NVARCHAR(50)

	DECLARE DataSources_cursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT ReportNumber, ReportName, DataSourceID, [Language] FROM @DataSources 
	OPEN DataSources_cursor  
	FETCH NEXT FROM DataSources_cursor INTO @ReportNumber, @ReportName, @DataSource, @Language; 

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		IF OBJECT_ID('tempdb..#EnglishDuplicates') IS NOT NULL
			DROP TABLE #EnglishDuplicates
		CREATE TABLE #EnglishDuplicates(
			ReportNumber INT NOT NULL,
			ReportName NVARCHAR(50) NOT NULL,
			TermID UNIQUEIDENTIFIER NOT NULL,
			TermHash VARBINARY(16) NOT NULL,
			DataSourceID NVARCHAR(50) NOT NULL,
			[Language] NVARCHAR(50) NOT NULL,
			RecordID UNIQUEIDENTIFIER NOT NULL) 

		IF OBJECT_ID('tempdb..#BilingualDuplicates') IS NOT NULL
			DROP TABLE #BilingualDuplicates
		CREATE TABLE #BilingualDuplicates(
			ReportNumber INT NOT NULL,
			ReportName NVARCHAR(50) NOT NULL,
			TermID UNIQUEIDENTIFIER NOT NULL,
			TermHash VARBINARY(16) NOT NULL,
			DataSourceID NVARCHAR(50) NOT NULL,
			[Language] NVARCHAR(50) NOT NULL,
			RecordID UNIQUEIDENTIFIER NOT NULL) 

		INSERT #EnglishDuplicates
			SELECT @ReportNumber, @ReportName, n.*
				FROM #TermsNormalized n 
				WHERE EXISTS
				(
					SELECT TermHash FROM #TermsNormalized WHERE DataSourceID = 'UNHQ'		AND [Language] = 'English' AND TermHash = n.TermHash
					INTERSECT
					SELECT TermHash FROM #TermsNormalized WHERE DataSourceID = @DataSource	AND [Language] = 'English' AND TermHash = n.TermHash
				)

		INSERT #BilingualDuplicates
			SELECT @ReportNumber, @ReportName, n.*
				FROM #TermsNormalized n 
				WHERE EXISTS
				(
					SELECT TermHash FROM #TermsNormalized WHERE DataSourceID = 'UNHQ'		AND [Language] = @Language AND TermHash = n.TermHash
					INTERSECT
					SELECT TermHash FROM #TermsNormalized WHERE DataSourceID = @DataSource	AND [Language] = @Language AND TermHash = n.TermHash
				)

		INSERT #Duplicates
			SELECT 'Perfect' AS DuplicateType, e.* 
			FROM #EnglishDuplicates e
			JOIN #BilingualDuplicates b 
				ON e.RecordID = b.RecordID

		INSERT #Duplicates
			SELECT 'Imperfect' AS DuplicateType, e.* 
			FROM #EnglishDuplicates e
			LEFT JOIN #BilingualDuplicates b 
				ON e.RecordID = b.RecordID
			WHERE b.RecordID IS NULL

		FETCH NEXT FROM DataSources_cursor INTO @ReportNumber, @ReportName, @DataSource, @Language; 
	END

	CLOSE DataSources_cursor;  
	DEALLOCATE DataSources_cursor;  

	-- Output for Excel files:
	;WITH 
		CTE_Terms AS
		(
			SELECT 
				o.*, t.Term, r.[Space], 
				CASE WHEN o.DuplicateType = 'Perfect' AND o.DataSourceID <> 'UNHQ' THEN 'deletedduplicate' ELSE NULL END AS [NewSpace],
				CONCAT(@Domain, '/UNTERM/Display/record/', o.DataSourceID, '/NA/', o.RecordID) AS RecordLink
			FROM #Duplicates o
			JOIN dbo.Term t ON t.TermID = o.TermID
			JOIN dbo.Record r ON r.RecordID = t.RecordID
		),
		CTE_Records AS
		(
			SELECT RecordID, RecordLink, DataSourceID, [Space], NewSpace, ReportNumber, ReportName, DuplicateType
			FROM CTE_Terms
			GROUP BY RecordID, RecordLink, DataSourceID, [Space], NewSpace, ReportNumber, ReportName, DuplicateType
		),
		CTE_English AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash EnglishHash, Term English FROM CTE_Terms WHERE [Language] = 'English'			
		),
		CTE_Russian AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash RussianHash, Term Russian FROM CTE_Terms WHERE [Language] = 'Russian'			
		),
		CTE_Arabic AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash ArabicHash, Term Arabic FROM CTE_Terms WHERE [Language] = 'Arabic'			
		),
		CTE_Chinese AS 
		(
			SELECT ReportNumber, DataSourceID, RecordID, TermHash ChineseHash, Term Chinese FROM CTE_Terms WHERE [Language] = 'Chinese'			
		),
		CTE_Report AS
		(
			SELECT DISTINCT
			f.DuplicateType [Duplicate Type],
			f.ReportNumber [Report Number], f.ReportName [Report Name],
			DENSE_RANK() OVER(PARTITION BY f.ReportNumber ORDER BY e.EnglishHash)	- 1	AS [English Group],

			f.DataSourceID AS [Database], 
			f.[Space], f.NewSpace,
			e.English AS English, 
			COALESCE(r.Russian, RussianTerms.Russian) AS Russian, 
			COALESCE(c.Chinese, ChineseTerms.Chinese) AS Chinese, 
			COALESCE(a.Arabic, ArabicTerms.Arabic) AS Arabic, 
			e.EnglishHash, r.RussianHash, a.ArabicHash, c.ChineseHash,
			[Subject].[Subject], [Body].[Body],
			f.RecordLink,
			f.RecordID 
			FROM CTE_Records f
			LEFT JOIN CTE_English	e ON e.DataSourceID = f.DataSourceID AND e.RecordID = f.RecordID AND e.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Russian	r ON r.DataSourceID = f.DataSourceID AND r.RecordID = f.RecordID AND r.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Arabic	a ON a.DataSourceID = f.DataSourceID AND a.RecordID = f.RecordID AND a.ReportNumber = f.ReportNumber
			LEFT JOIN CTE_Chinese	c ON c.DataSourceID = f.DataSourceID AND c.RecordID = f.RecordID AND c.ReportNumber = f.ReportNumber
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(s.[Name], ', ')
					FROM dbo.[Subject] s
					JOIN  dbo.RecordSubject rs 
						ON rs.SubjectID = s.SubjectID
					WHERE rs.RecordID = f.RecordID
					FOR XML PATH('') 
					) [Subject]
				) [Subject]
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(b.[Name], ', ')
					FROM dbo.Body b
					JOIN  dbo.RecordBody rb 
						ON rb.BodyID = b.BodyID
					WHERE rb.RecordID = f.RecordID
					FOR XML PATH('') 
					) [Body]
				) [Body]
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Russian'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Russian
				) RussianTerms
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Chinese'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Chinese
				) ChineseTerms
			OUTER APPLY (
				SELECT (
					SELECT CONCAT(t.Term, ', ')
					FROM dbo.Record r
					JOIN  dbo.Term t 
						ON t.RecordID = r.RecordID
						AND t.[Language] = 'Arabic'
					WHERE r.RecordID = f.RecordID
						AND	t.[Type] NOT IN ('acronym') -- full, abbrv, acronym, phrase, short, suggestion, term, title, variant
					ORDER BY t.[Type]
					FOR XML PATH('') 
					) Arabic
				) ArabicTerms
		)
		INSERT @Output
		SELECT 
			 'Report' AS OutputDescription
			,[Duplicate Type]
			,[Report Number]
			,[Report Name]	
			,[English Group]
			,[Database]	
			,[Space]
			,NewSpace	
			,English
			,Russian		
			,Arabic
			,Chinese	
			,[Subject]
			,Body
			,RecordLink
			,RecordID
			,NULL,NULL,NULL,NULL
			FROM CTE_Report r
			WHERE r.EnglishHash IS NOT NULL
			ORDER BY r.[Report Number], r.[English Group], CASE r.[Database] WHEN 'UNHQ' THEN NULL ELSE r.[Database] END, r.RecordLink

	print ''
	print 'dbo.Record update, set [Space] to deletedduplicate for Perfect Duplicates'

	DECLARE @CurrentUtcDate DATETIME = GETUTCDATE()

	DECLARE @EmergencyRollbackOutput TABLE(
		[RecordID] [uniqueidentifier] NOT NULL,
		[deleted_Space] [nvarchar] (50) NULL,
		[deleted_Updated] [datetime] NULL,
		[inserted_Space] [nvarchar] (50) NULL,
		[inserted_Updated] [datetime] NULL
	)

	UPDATE dbo.Record SET
--	SELECT
		[Space] = 'deletedduplicate',
		Updated = @CurrentUtcDate
		OUTPUT 
			deleted.RecordID, 
			deleted.[Space], deleted.Updated,
			inserted.[Space], inserted.Updated 
			INTO @EmergencyRollbackOutput
		FROM dbo.Record r
		JOIN #Duplicates d 
			ON d.RecordID = r.RecordID
		WHERE d.DuplicateType = 'Perfect' COLLATE SQL_Latin1_General_CP1_CI_AS
		AND d.DataSourceID COLLATE SQL_Latin1_General_CP1_CI_AS IN (SELECT DataSourceID From @DataSources)

	INSERT @Output
	(
		OutputDescription,
		RecordID,
		[deleted_Space],
		[deleted_Updated],
		[inserted_Space],
		[inserted_Updated]
	)
	SELECT 
		'EmergencyRollback' AS OutputDescription
		,* FROM @EmergencyRollbackOutput;

	IF OBJECT_ID('tempdb..#TermsNormalized') IS NOT NULL
		DROP TABLE #TermsNormalized

	IF OBJECT_ID('tempdb..#Duplicates') IS NOT NULL
		DROP TABLE #Duplicates

	IF OBJECT_ID('tempdb..#EnglishDuplicates') IS NOT NULL
		DROP TABLE #EnglishDuplicates

	IF OBJECT_ID('tempdb..#BilingualDuplicates') IS NOT NULL
		DROP TABLE #BilingualDuplicates

	IF OBJECT_ID('tempdb..#Duplicates') IS NOT NULL
		DROP TABLE #Duplicates

	SELECT * FROM @Output
	ORDER BY OutputDescription DESC, [Report Number], [English Group], CASE [Database] WHEN 'UNHQ' THEN NULL ELSE [Database] END, RecordLink

ROLLBACK
-- COMMIT

/*
-- Restore from snapshot after start script if needed:
USE master
GO
ALTER DATABASE [UNTERM3] SET OFFLINE WITH ROLLBACK IMMEDIATE
GO
ALTER DATABASE [UNTERM3] SET ONLINE
GO
RESTORE DATABASE UNTERM3 FROM DATABASE_SNAPSHOT = 'UNTERM3_SNAPSHOT';  
GO  
*/



