
# Please, set parameters below (ServerInstance, Database, Username and Password):

$ServerInstance = "."
$Database = "UNTERM3"
$Username = "gtextdev"
$Password = "^[~A(rjWc;&4"

$InputFilePath = ".\US111707 sql query - v5.sql"
$OutputFilePath = ".\US111707 sql query - v5.csv"

Import-Module "Microsoft.PowerShell.Management"
if ([System.IO.File]::Exists($OutputFilePath))
{
	Remove-Item $OutputFilePath
}

$SqlQuery = [IO.File]::ReadAllText($InputFilePath) 

#echo $SqlQuery

Import-Module "sqlps"
Invoke-Sqlcmd -ServerInstance $ServerInstance -Database $Database -Username $Username -Password $Password -QueryTimeout 600 -Query $SqlQuery | Export-CSV -Encoding UTF8 -NoTypeInformation $OutputFilePath
